package com.generation.generator;

import java.io.ByteArrayInputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class GeneratorController {

  @Autowired
  private GeneratorService generatorService;

  @RequestMapping(value = "/download", method = RequestMethod.GET, produces = "application/json")
  public ResponseEntity<InputStreamResource> download(@RequestParam String amount) {
    byte[] buf = generatorService.generatePoints(Integer.parseInt(amount));

    return ResponseEntity.ok().contentLength(buf.length)
        .contentType(MediaType.parseMediaType("application/octet-stream"))
        .header("Content-Disposition", "attachment; filename=\"data.json\"")
        .body(new InputStreamResource(new ByteArrayInputStream(buf)));
  }
}
