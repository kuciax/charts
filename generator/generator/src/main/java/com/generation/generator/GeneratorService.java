package com.generation.generator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import org.springframework.stereotype.Service;

@Service
public class GeneratorService {

  public byte[] generatePoints(int amount) {
    List<Point> points = getPoints(amount);

    ObjectMapper objectMapper = new ObjectMapper();

    try {
      return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsBytes(points);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
    return null;
  }

  private List<Point> getPoints(int amount) {
    double minValue = 5.0;
    double maxValue = 10000.0;
    List<Point> points = new ArrayList<>(amount);
    for (int i = 0; i <= amount; i++) {
      Double randomX = BigDecimal
          .valueOf(ThreadLocalRandom.current().nextDouble(minValue, maxValue))
          .setScale(2, RoundingMode.UP).doubleValue();
      Double randomY = BigDecimal
          .valueOf(ThreadLocalRandom.current().nextDouble(minValue, maxValue))
          .setScale(2, RoundingMode.UP).doubleValue();
      points.add(new Point(randomX, randomY));
    }
    return points;
  }
}
