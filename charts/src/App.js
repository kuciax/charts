import React, { Component } from "react";
import "./App.css";
import Scatter from "./Scatter/Scatter";
import Histogram from "./Histogram/Histogram";
import Rain from "./Rain/Rain";
import Density from "./Density/Density";
import SubPlot from "./Scatter/SubPlot";

//Subplot - works without backend
//others you need backend
class App extends Component {
  render() {
    return (
      <div className="App">
        <h1>Charts:</h1>
        <SubPlot />
        {/* <Scatter />
        <Density />
        <Histogram />
        <Rain /> */}
      </div>
    );
  }
}

export default App;
