import React, { Component } from 'react';
import Plotly from 'plotly.js-dist';
import createPlotlyComponent from 'react-plotly.js/factory';

export default class Density extends Component {
    Plot = createPlotlyComponent(Plotly);
    url = 'http://localhost:8181/download?amount=';
    amount = 80000;
    chartWidth = 800;
    chartHeight = 600;
    type = 'histogram2dcontour';
    mode = 'markers';
    startTime;
    graphDiv;
    colors = ['orange', '#76deff', '#797979', '#00cb00'];

    defaultMarker = {
        color: this.colors[0],
        opacity: 0.6,
        line: {
            width: 1,
            color: 'rgb(0,0,0)'
        },
    };
    layout = {
        width: this.chartWidth,
        height: this.chartHeight,
        title: 'Density - CSS style to disable BG: g.contourbg{display:none}',
        spikedistance: Math.max(this.chartWidth, this.chartHeight),
        hovermode: 'closest',
        xaxis: {
            rangemode: 'tozero',
            title: 'X axis title',
            showspikes: true,
            spikethickness: 1,
            spikedash: 'dot',
            spikemode: "across",
            spikesnap: 'cursor',
        },
        yaxis: {
            rangemode: 'tozero',
            title: 'Y axis title',
            showspikes: true,
            spikethickness: 1,
            spikedash: 'dot',
            spikemode: "across",
            spikesnap: 'cursor',
        },
        shapes: [],
        annotations: [this.annotation],
    };

    traceConfiguration = {
        type: this.type,
        colorscale: "Rainbow",
        name: 'Density',
        showlegend: false,
        //hoverinfo: "x+y+text",
        ncontours: 20,
        showscale: false,
        x: [],
        y: [],
    };
    traces = [];

    state = {
        revision: 0,
    };

    densityData = {x: [], y: []};

    componentDidMount() {
        this.startTime = new Date().getTime();

        const series = this.generateDensitySeries();
        for (let i = 0; i < series.length; i ++) {
            this.traces.push(this.createTrace(series[i], 0))
        }

        console.log('component did mount.');
    }


    generateDensitySeries() {
        const densityLevels = 10;
        const rangeForX = [
            [5800, 7400],
            [1800, 3800],
            [1200, 3000],
            [5000, 6600],
        ];
        const rangeForY = [
            [8000, 11000],
            [8000, 11000],
            [1500, 3000],
            [1500, 3000],
        ];
        const seriesCount = rangeForX.length;
        const series = [];
        for(let i = 0; i < seriesCount; i ++) {
            series.push({
                x: [],
                y: [],
            })
        }

        // y = x, a = 1
        // y = -x + 10, a = -1
        let a;
        let b;
        let gen;
        for (var i  = 1; i < densityLevels + 1; i++){
            if (i <= 5) {
                a = 1;
                b = 0;
            } else {
                a = -1;
                b = 10;
            }
            gen = (a * i + b + 1);
            gen = Math.pow(gen, 3);
            console.log(gen);
            for (let j = 0; j < seriesCount; j ++) {
                let xVal = rangeForX[j][0] + (rangeForX[j][1] - rangeForX[j][0]) / densityLevels * i;
                xVal = xVal + parseInt(Math.random()*100-50);
                let yVal = rangeForY[j][0] + (rangeForY[j][1] - rangeForY[j][0]) / densityLevels * i;
                yVal = yVal + parseInt(Math.random()*100-50);
                for (let k = 0; k < gen; k ++) {
                    series[0].x.push(xVal);
                    series[0].y.push(yVal);
                }
            }

        }
        return series;
    }

    createTrace(points, index) {
        const traceMarker = Object.assign({}, this.defaultMarker);

        const trace = Object.assign(
            {},
            this.traceConfiguration,
            {
                x: points.x,
                y: points.y,
                marker: traceMarker,
            });
        return trace;
    }

    onInitialized(figure, graphDiv) {
        console.log('onInitialized');
        this.graphDiv = graphDiv;
    }

    render() {
        const plot1 = React.createElement(this.Plot, {
            data: this.traces,
            layout: this.layout,
            onInitialized: (figure, graphDiv) => {this.onInitialized(figure, graphDiv)},
            key: 'plot1',
            //revision: this.state.revision,
        });
        const container = React.createElement('div', {}, [plot1]);
        return container;
    }
}