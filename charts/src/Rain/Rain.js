import React, { Component } from 'react';
import Plotly from 'plotly.js-dist';
import createPlotlyComponent from 'react-plotly.js/factory';

export default class Rain extends Component {
    Plot = createPlotlyComponent(Plotly);
    url = 'http://localhost:8181/download?amount=';
    chartWidth = 800;
    chartHeight = 600;
    type = 'scattergl';
    mode = 'markers';
    startTime;
    graphDiv;
    colors = ['orange', '#76deff'];

    annotation = {
        visible: false,
        bordercolor: '#FF9900',
        borderpad: 5,
        bgcolor: '#FFCC99',
        opacity: 0.8,
        text: 'annotation text',
        x: 0,
        y: 0,
        showarrow: false,
        xanchor: 'left',
        xshift: 5,
        yshift: 5,
        yanchor: 'bottom',
        align: 'bottom',
    };
    defaultMarker = {
        color: this.colors[0],
        opacity: 0.6,
        line: {
            width: 1,
            color: 'rgb(0,0,0)'
        },
    };
    layout = {
        width: this.chartWidth,
        height: this.chartHeight,
        title: '20k points for each trace',
        spikedistance: Math.max(this.chartWidth, this.chartHeight),
        hovermode: 'closest',
        xaxis: {
            rangemode: 'tozero',
            title: 'X axis title',
            showspikes: true,
            spikethickness: 1,
            spikedash: 'dot',
            spikemode: "across",
            spikesnap: 'cursor',
        },
        yaxis: {
            rangemode: 'tozero',
            title: 'Y axis title',
            showspikes: true,
            spikethickness: 1,
            spikedash: 'dot',
            spikemode: "across",
            spikesnap: 'cursor',
        },
        shapes: [],
        annotations: [this.annotation],
    };

    traceConfiguration = {
        text: [],
        marker: this.defaultMarker,
        type: this.type,
        mode: this.mode,
        name: 'Scatter 1',
        showlegend: false,
        hoverinfo: "x+y+text",
        x: [],
        y: [],
    };
    traces = [];

    componentDidMount() {
        this.startTime = new Date().getTime();
        Promise.all([
            this.getDataFromServer(20000),
            this.getDataFromServer(20000),
        ]).then((data) => {
            console.log('LOADING DATA FROM SERVER:', (new Date().getTime() - this.startTime), 'ms');
            let result1 = this.parseDataFromServer(data[0]);
            let result2 = this.parseDataFromServer(data[1]);
            result1 = this.tempMethodToImitateRainData(result1, 'up');
            result2 = this.tempMethodToImitateRainData(result2, 'down');

            this.traces = [
                this.createTrace(result1, 0),
                this.createTrace(result2, 1),
            ];
            this.graphDiv.data = this.traces;
            Plotly.redraw(this.graphDiv);
        });
        console.log('component did mount.');
    }

    getDataFromServer(amount) {
        return fetch(`${this.url}${amount}`)
            .then(res => res.json())
    }

    parseDataFromServer(dataArray) {
        const result = {
            x: [],
            y: [],
        };
        dataArray.forEach((data) => {
            result.x.push(data.x);
            result.y.push(data.y);
        });
        return result;
    }

    tempMethodToImitateRainData(data, scaleMethod) {
        switch(scaleMethod) {
            case 'up':
                data.y.forEach((y, index, array) => {
                    array[index] = y / 5 + 15000;
                });
                break;
            case 'down':
                data.y.forEach((y, index, array) => {
                    array[index] = y / 5 + 1000;
                });
                break;
        }
        return data;
    }

    getValuesFromCursor(event) {
        const xaxis = this.getAxis('x');
        const yaxis = this.getAxis('y');
        const left = this.graphDiv._fullLayout.margin.l;
        const top = this.graphDiv._fullLayout.margin.t;
        const clickX = event.layerX;
        const clickY = event.layerY;
        const xVal = xaxis.p2c(clickX - left);
        const yVal = yaxis.p2c(clickY - top);
        if (xVal < xaxis.range[0] || xVal > xaxis.range[1] || yVal < yaxis.range[0] || yVal > yaxis.range[1]) {
            return {
                x: NaN,
                y: NaN,
            };
        }
        return {
            x: xVal,
            y: yVal,
        };
    }

    getAxis(axisName) {
        switch(axisName) {
            case 'x':
                return this.graphDiv._fullLayout.xaxis;
            case 'y':
                return this.graphDiv._fullLayout.yaxis;
        }
    }

    drawLabelForCursor(event) {
        this.startTime = new Date().getTime();
        const xy = this.getValuesFromCursor(event);
        if (!isNaN(xy.x)) {
            this.annotation.visible = true;
            this.annotation.x = xy.x;
            this.annotation.y = xy.y;
            this.annotation.text = `(${xy.x.toFixed(2)}, ${xy.y.toFixed(2)})`;
            Plotly.relayout(this.graphDiv, {annotations: [this.annotation]});
        }
    }

    createTrace(points, index) {
        const traceMarker = Object.assign({}, this.defaultMarker);
        traceMarker.color = this.colors[index];
        const trace = Object.assign(
            {},
            this.traceConfiguration,
            {
                x: points.x,
                y: points.y,
                marker: traceMarker,
            });
        return trace;
    }

    onInitialized(figure, graphDiv) {
        console.log('onInitialized');
        this.graphDiv = graphDiv;

        graphDiv.addEventListener('mousemove', (event) => {
            this.drawLabelForCursor(event);
        });
    }

    onUpdate(figure, graphDiv) {
        console.log('onUpdate', 'RENDERING DATA TOOK:', (new Date().getTime() - this.startTime), 'ms');
    }

    render() {
        const plot1 = React.createElement(this.Plot, {
            data: this.traces,
            layout: this.layout,
            onUpdate: (figure, graphDiv) => {this.onUpdate(figure, graphDiv)},
            onInitialized: (figure, graphDiv) => {this.onInitialized(figure, graphDiv)},
            key: 'plot1',
        });
        const container = React.createElement('div', {}, [plot1]);
        return container;
    }
}