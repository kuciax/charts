import React, { Component } from 'react';
import Plotly from 'plotly.js-dist';
import createPlotlyComponent from 'react-plotly.js/factory';

export default class Histogram extends Component {
    Plot = createPlotlyComponent(Plotly);
    url = 'http://localhost:8181/download?amount=';
    amount = 80000;
    chartWidth = 800;
    chartHeight = 600;
    type = 'scattergl';
    startTime;
    graphDiv;
    colors = ['orange', '#76deff', '#797979', '#00cb00'];


    layout = {
        width: this.chartWidth,
        height: this.chartHeight,
        title: '1D Histogram',
        spikedistance: Math.max(this.chartWidth, this.chartHeight),
        hovermode: 'closest',
        xaxis: {
            title: "Fluorescene intensity [RFU]",
            rangeslider: {
                visible: true,
            }
        },
        yaxis: {
            title: "Count",
            type: 'linear',
        },
        updatemenus: [
            {
                type: 'buttons',
                direction: 'left',
                x: 0.1,
                xanchor: 'left',
                y: 1.1,
                yanchor: 'top',
                buttons: [{
                    args:['yaxis.type', 'linear'],
                    label: 'Y linear scale',
                    method:'relayout',
                },{
                    args:['yaxis.type', 'log'],
                    label: 'Y log scale',
                    method:'relayout',
                }],
            },
        ]
    };

    dataFromDB = [];


    histogramData = {
        x: [],
        type: 'histogram',
        xbins: {
            size: 100,
            start: 0,
            end: 22000,
        },
    };

    componentDidMount() {
        this.startTime = new Date().getTime();
        /*this.getDataFromServer().then(dataArray => {
            console.log('LOADING DATA FROM SERVER:', (new Date().getTime() - this.startTime), 'ms');
            this.parseDataFromServer(dataArray);
            //let filteredData = this.filterDataForRange(this.initialXRange, this.initialYRange);
            //filteredData = this.limitData(filteredData);
            this.startTime = new Date().getTime();
            //this.createInitialChartData(this.dataFromDB);
            const xy = this.createInitialChartPoints(this.dataFromDB);
            this.traces = [this.createTrace(xy, 0)];
            this.graphDiv.data = this.traces;
            //Plotly.react (this.graphDiv, {data: this.traces});
            //Plotly.restyle(this.graphDiv, {data: this.traces});
            this.setInitialRange();
            Plotly.redraw(this.graphDiv);
            this.lockRange();
            //this.rerenderChart();
        });*/
        this.generateHistogramData();
        console.log('component did mount.');
    }

    generateHistogramData() {
        for (let i = 0; i < 100; i ++) {
            this.histogramData.x.push(1500);
            this.histogramData.x.push(2500);
        }
        for (let i = 0; i < 200; i ++) {
            this.histogramData.x.push(1600);
            this.histogramData.x.push(2400);
        }
        for (let i = 0; i < 350; i ++) {
            this.histogramData.x.push(1700);
            this.histogramData.x.push(2300);
        }
        for (let i = 0; i < 550; i ++) {
            this.histogramData.x.push(1800);
            this.histogramData.x.push(2200);
        }
        for (let i = 0; i < 620; i ++) {
            this.histogramData.x.push(1900);
            this.histogramData.x.push(2100);
        }
        for (let i = 0; i < 650; i ++) {
            this.histogramData.x.push(2000);
        }

        for (let i = 0; i < 50; i ++) {
            this.histogramData.x.push(15000);
            this.histogramData.x.push(16000);
        }
        for (let i = 0; i < 100; i ++) {
            this.histogramData.x.push(15100);
            this.histogramData.x.push(15900);
        }
        for (let i = 0; i < 175; i ++) {
            this.histogramData.x.push(15200);
            this.histogramData.x.push(15800);
        }
        for (let i = 0; i < 225; i ++) {
            this.histogramData.x.push(15300);
            this.histogramData.x.push(15700);
        }
        for (let i = 0; i < 310; i ++) {
            this.histogramData.x.push(15400);
            this.histogramData.x.push(15600);
        }
        for (let i = 0; i < 325; i ++) {
            this.histogramData.x.push(15500);
        }
    }

    setInitialRange() {
        this.layout.xaxis.range = [0, this.getMaxForAxis('x')];
        this.layout.yaxis.range = [0, this.getMaxForAxis('y')];
    }

    lockRange() {
        this.layout.xaxis.autorange = false;
        this.layout.yaxis.autorange = false;
    }

    getMinForAxis(axisName) {
        let min = Number.POSITIVE_INFINITY;
        this.dataFromDB.forEach((data) => {
            if (data[axisName] < min) {
                min = data[axisName];
            }
        });
        return min;
    }

    getMaxForAxis(axisName) {
        let max = Number.NEGATIVE_INFINITY;
        this.dataFromDB.forEach((data) => {
            if (data[axisName] > max) {
                max = data[axisName];
            }
        });
        return max;
    }

    getValuesFromCursor(event) {
        const xaxis = this.getAxis('x');
        const yaxis = this.getAxis('y');
        const left = this.graphDiv._fullLayout.margin.l;
        const top = this.graphDiv._fullLayout.margin.t;
        const clickX = event.layerX;
        const clickY = event.layerY;
        const xVal = xaxis.p2c(clickX - left);
        const yVal = yaxis.p2c(clickY - top);
        if (xVal < xaxis.range[0] || xVal > xaxis.range[1] || yVal < yaxis.range[0] || yVal > yaxis.range[1]) {
            return {
                x: NaN,
                y: NaN,
            };
        }
        return {
            x: xVal,
            y: yVal,
        };
    }

    getAxis(axisName) {
        switch(axisName) {
            case 'x':
                return this.graphDiv._fullLayout.xaxis;
            case 'y':
                return this.graphDiv._fullLayout.yaxis;
        }
    }

    drawLabelForCursor(event) {
        this.startTime = new Date().getTime();
        const xy = this.getValuesFromCursor(event);
        if (!isNaN(xy.x)) {
            this.annotation.visible = true;
            this.annotation.x = xy.x;
            this.annotation.y = xy.y;
            this.annotation.text = `(${xy.x.toFixed(2)}, ${xy.y.toFixed(2)})`;
            Plotly.relayout(this.graphDiv, {annotations: [this.annotation]});
        }
    }

    onInitialized(figure, graphDiv) {
        console.log('onInitialized');
        this.graphDiv = graphDiv;

        graphDiv.addEventListener('mousemove', (event) => {
            console.log('mousemove');
            //this.drawLabelForCursor(event);
        });
        graphDiv.addEventListener('click', (event) => {
            console.log('click');
            //this.drawThreshold(event);
        });
        graphDiv.on('plotly_relayout', (plotData) => {
            console.log('plotly_relayout');
            //this.onZoom(plotData);
        })

        graphDiv.on('plotly_click', (...args) => {
            console.log('plotly_click');
            //this.onPointClick(args);
        })
        graphDiv.on('plotly_hover', (event) => {
            console.log('plotly_hover');
            //this.onHover(event);
        })
        graphDiv.on('plotly_unhover', (event) => {
            console.log('plotly_unhover');
            //this.onUnHover(event);
        })
        graphDiv.on('plotly_doubleclick', (plotData) => {
            console.log('plotly_doubleclick');
        })
    }

    onUpdate(figure, graphDiv) {
        console.log('onUpdate', 'RENDERING DATA TOOK:', (new Date().getTime() - this.startTime), 'ms');
    }

    render() {
        const plot2 = React.createElement(this.Plot, {
            data: [this.histogramData],
            layout: {
                title: '1D Histogram',
                xaxis: {
                    title: "Fluorescene intensity [RFU]",
                    rangeslider: {
                        visible: true,
                    }
                },
                yaxis: {
                    title: "Count",
                    type: 'linear',
                },
                width: 800, height: 600,
                updatemenus: [
                    {
                        type: 'buttons',
                        direction: 'left',
                        x: 0.1,
                        xanchor: 'left',
                        y: 1.1,
                        yanchor: 'top',
                        buttons: [{
                            args:['yaxis.type', 'linear'],
                            label: 'Y linear scale',
                            method:'relayout',
                        },{
                            args:['yaxis.type', 'log'],
                            label: 'Y log scale',
                            method:'relayout',
                        }],
                    }
                ]
            },
            //onUpdate: (figure, graphDiv) => {this.onUpdate(figure, graphDiv)},
            key: 'plot2',
        });
        const container = React.createElement('div', {}, [plot2]);
        return container;
    }
}