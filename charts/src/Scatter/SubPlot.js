import React from "react";
import Plot from "react-plotly.js";
import { Plotly } from "plotly.js-dist";
import styled from "styled-components";

export default class SubPlot extends React.Component {
  trace1 = {
    x: [],
    y: [],
    type: "scatter",
    mode: "markers",
    xaxis: "x",
    yaxis: "y"
  };

  trace2 = {
    x: [2, 3, 4],
    y: [5, 5, 5],
    mode: "markers",
    xaxis: "x2",
    yaxis: "y",
    type: "scatter"
  };

  trace3 = {
    x: [2, 3, 4],
    y: [600, 700, 800],
    xaxis: "x",
    yaxis: "y3",
    type: "scatter"
  };

  trace4 = {
    x: [4000, 5000, 6000],
    y: [7000, 8000, 9000],
    xaxis: "x4",
    yaxis: "y4",
    type: "scatter"
  };

  layout = {
    grid: {
      rows: 2,
      columns: 2,
      subplots: [["xy", "x2y"], ["xy3", "x4y4"]],
      xgap: 0.01 // distance between two columns in grid
    },
    title: {
      text: "Plot Title",
      font: {
        family: "Courier New, monospace",
        size: 24
      },
      xref: "paper",
      x: 0.05
    },
    width: 1000,
    height: 1000,
    xaxis: { range: [0, 100], domain: [0, 0.5] },
    xaxis2: { range: [0, 100] },
    yaxis: { rannge: [0, 40] },
    shapes: [
      {
        layer: "below",
        type: "line",
        opacity: 0.5,
        yref: "y", // this shape refers to y axis
        xref: "x", // same with x
        x0: -300,
        x1: 300,
        y0: 0,
        y1: 0,
        line: {
          color: "rgb(255, 0, 0)",
          width: 3
        }
      },
      {
        layer: "below",
        type: "line",
        opacity: 0.5,
        yref: "y",
        xref: "x2",
        x0: -300,
        x1: 300,
        y0: 0,
        y1: 0,
        line: {
          color: "rgb(255, 0, 0)",
          width: 3
        }
      }
    ]
  };
  data = [this.trace1, this.trace2, this.trace3, this.trace4]; //data for all shapes

  onRelayout(event) {
    console.log(event);
  }

  buttonClicked(event) {
    console.log(event);
  }

  componentDidMount() {
    console.log(this.layout);
    console.log(this.layout.shapes);
    const lengthX = 100;
    const lengthY = 100;
    // filling arrays by random numbers
    this.trace1.y = Array.from({ length: lengthX }, () =>
      Math.floor(Math.random() * lengthY)
    );
    this.trace1.y.forEach((item, index) => this.trace1.x.push(index));
    this.trace2.y = Array.from({ length: lengthX }, () =>
      Math.floor(Math.random() * lengthY)
    );
    this.trace2.y.forEach((item, index) => this.trace2.x.push(index));
  }

  render() {
    return (
      <div className={this.props.className}>
        <div className="plot">
          {/* test some events */}
          <Plot
            data={this.data}
            layout={this.layout}
            config={{ edits: { shapePosition: true } }}
            onButtonClicked={e => console.log("onBtnCLick", e)}
            onClick={e => console.log("onCLick", e)}
            onClickAnnotation={e => console.log("onCLickAnnotation", e)}
            onFramework={e => console.log("onFramework", e)}
            onRedraw={e => console.log("onRedraw", e)}
            onRelayout={e => console.log("onRelayout", e)}
            onHover={e => console.log("onHover", e)}
            onTransitioning={e => console.log("onTransitioning", e)}
            onAnimatingFrame={e => console.log("onAnimatingFrame", e)}
          />
        </div>
        <div className="input">
          <input />
        </div>
      </div>
    );
  }
}

export const StyledSubPlot = styled(SubPlot)`
  overflow: scroll;
  width: 5000px;
  div.modebar {
    margin-right: 2000px;
    width: 300px;
    border: 1px solid;
    outline: 2px dashed blue;
  }
  position: relative;
  width: 1000px;
  height: 1000px;
  .plot,
  .input {
    position: absolute;
    top: 0;
    left: 0;
  }
  .input {
    z-index: 10;
    margin: 200px;
  }
`;
