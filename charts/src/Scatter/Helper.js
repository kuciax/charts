import React from "react";
import styled from "styled-components";
export default class Helper extends React.Component {
  componentDidMount() {}
  render() {
    return (
      <div
        className={this.props.className}
        onClick={(e) => console.log("click!", e)}
      >
        Helper
      </div>
    );
  }
}

export const StyledHelper = styled(Helper)`
  width: 1000px;
  height: 1000px;
`;
