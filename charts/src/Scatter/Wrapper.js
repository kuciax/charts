import React from "react";
import styled from "styled-components";
import { StyledSubPlot as SubPlot } from "./SubPlot";
import { StyledHelper as Helper } from "./Helper";
export default class Wrapper extends React.Component {
  componentDidMount() {}
  render() {
    return (
      <div
        className={this.props.className}
        onClick={() => console.log("click!")}
      >
        <SubPlot />
      </div>
    );
  }
}

export const StyledWrapper = styled(Wrapper)``;
