import React, { Component } from "react";
import Plotly from "plotly.js-dist";
import createPlotlyComponent from "react-plotly.js/factory";
export default class Scatter extends Component {
  Plot = createPlotlyComponent(Plotly);
  maxDataToDisplay = 500000;
  url = "http://localhost:8181/download?amount=";
  amount = 20000;
  chartWidth = 800;
  chartHeight = 600;
  type = "scattergl";
  mode = "markers";
  startTime;
  graphDiv;
  showThresholdForX = true;
  showThresholdForY = true;
  colors = ["orange", "#76deff", "#797979", "#00cb00"];

  annotation = {
    visible: false,
    bordercolor: "#FF9900",
    borderpad: 5,
    bgcolor: "#FFCC99",
    opacity: 0.8,
    text: "annotation text",
    x: 0,
    y: 0,
    //ax: 10,
    //ay: -20,
    showarrow: false,
    xanchor: "left",
    xshift: 5,
    yshift: 5,
    yanchor: "bottom",
    align: "bottom"
  };
  defaultMarker = {
    color: this.colors[0],
    opacity: 0.3,
    line: {
      width: 1,
      color: "rgb(0,0,0)"
    }
  };
  thresholdX = {
    type: "line",
    x0: 0,
    y0: 0,
    x1: 0,
    y1: 0,
    line: {
      color: "#ff00ff",
      width: 3
    }
  };
  thresholdY = {
    type: "line",
    x0: 0,
    y0: 0,
    x1: 0,
    y1: 0,
    line: {
      color: "#ff00ff",
      width: 3
    }
  };
  layout = {
    width: this.chartWidth,
    height: this.chartHeight,
    title: "20k points + thresholds",
    spikedistance: Math.max(this.chartWidth, this.chartHeight),
    hovermode: "closest",
    xaxis: {
      rangemode: "tozero",
      title: "X axis title",
      showspikes: true,
      spikethickness: 1,
      spikedash: "line",
      spikemode: "across",
      spikesnap: "cursor"
    },
    shapes: [
      {
        type: "line",
        editable: true,
        yref: "paper",
        x0: 4000,
        x1: 4000,
        y0: 0,
        y1: 10000,
        line: {
          color: "rgb(255, 0, 0)",
          width: 50
        }
      }
    ],
    annotations: [this.annotation]
  };

  dataFromDB = [];

  traceConfiguration = {
    text: [],
    marker: this.defaultMarker,
    type: this.type,
    mode: this.mode,
    name: "Scatter 1",
    showlegend: false,
    hoverinfo: "x+y+text",
    x: [],
    y: []
  };
  traces = [];

  state = {
    revision: 0
  };

  componentDidMount() {
    this.startTime = new Date().getTime();
    this.getDataFromServer().then(dataArray => {
      console.log(
        "LOADING DATA FROM SERVER:",
        new Date().getTime() - this.startTime,
        "ms"
      );
      this.parseDataFromServer(dataArray);
      //let filteredData = this.filterDataForRange(this.initialXRange, this.initialYRange);
      //filteredData = this.limitData(filteredData);
      this.startTime = new Date().getTime();
      //this.createInitialChartData(this.dataFromDB);
      const xy = this.createInitialChartPoints(this.dataFromDB);
      this.traces = [this.createTrace(xy, 0)];
      this.graphDiv.data = this.traces;
      //Plotly.react (this.graphDiv, {data: this.traces});
      //Plotly.restyle(this.graphDiv, {data: this.traces});
      this.setInitialRange();
      Plotly.addTraces(this.graphDiv, this.traces);
      //Plotly.redraw(this.graphDiv);
      this.lockRange();
      //this.rerenderChart();
    });
    //this.generateHistogramData();
    console.log("component did mount.");
  }

  getDataFromServer() {
    return fetch(`${this.url}${this.amount}`).then(res => res.json());
  }

  parseDataFromServer(dataArray) {
    dataArray.forEach(data => {
      this.dataFromDB.push({
        x: data.x,
        y: data.y
      });
    });
  }

  setInitialRange() {
    this.layout.xaxis.range = [0, this.getMaxForAxis("x")];
    this.layout.yaxis.range = [0, this.getMaxForAxis("y")];
  }

  lockRange() {
    this.layout.xaxis.autorange = false;
    this.layout.yaxis.autorange = false;
  }

  getMinForAxis(axisName) {
    let min = Number.POSITIVE_INFINITY;
    this.dataFromDB.forEach(data => {
      if (data[axisName] < min) {
        min = data[axisName];
      }
    });
    return min;
  }

  getMaxForAxis(axisName) {
    let max = Number.NEGATIVE_INFINITY;
    this.dataFromDB.forEach(data => {
      if (data[axisName] > max) {
        max = data[axisName];
      }
    });
    return max;
  }

  filterDataForRange(xRange, yRange) {
    return this.dataFromDB.filter(data => {
      return (
        data.x >= xRange[0] &&
        data.x <= xRange[1] &&
        data.y >= yRange[0] &&
        data.y <= yRange[1]
      );
    });
  }

  limitData(data) {
    const limitedData = [];
    const dataCount = data.length;
    const step = Math.floor(dataCount / this.maxDataToDisplay);
    if (step > 1) {
      for (let i = 0; i < dataCount; i += step) {
        limitedData.push(data[i]);
      }
      return limitedData;
    }
    return data;
  }

  createInitialChartPoints(limitedData) {
    const x = [];
    const y = [];
    limitedData.forEach(data => {
      x.push(data.x);
      y.push(data.y);
    });
    return { x: x, y: y };
  }

  rerenderChart() {
    console.log("rerenderChart");
    this.setState({ revision: this.state.revision + 1 });
  }

  onZoom(plotData) {
    /*const xFrom = plotData['xaxis.range[0]'];
        const xTo = plotData['xaxis.range[1]'];
        const yFrom = plotData['yaxis.range[0]'];
        const yTo = plotData['yaxis.range[1]'];
        this.layout.xaxis.range = [xFrom, xTo];
        this.layout.yaxis.range = [yFrom, yTo];
        let filteredData = this.filterDataForRange([xFrom, xTo], [yFrom, yTo]);*/
    //filteredData= this.limitData(filteredData);
    //this.createChartData(filteredData);
    //this.rerenderChart();
  }

  getValuesFromCursor(event) {
    const xaxis = this.getAxis("x");
    const yaxis = this.getAxis("y");
    const left = this.graphDiv._fullLayout.margin.l;
    const top = this.graphDiv._fullLayout.margin.t;
    const clickX = event.layerX;
    const clickY = event.layerY;
    const xVal = xaxis.p2c(clickX - left);
    const yVal = yaxis.p2c(clickY - top);
    if (
      xVal < xaxis.range[0] ||
      xVal > xaxis.range[1] ||
      yVal < yaxis.range[0] ||
      yVal > yaxis.range[1]
    ) {
      return {
        x: NaN,
        y: NaN
      };
    }
    return {
      x: xVal,
      y: yVal
    };
  }

  getAxis(axisName) {
    switch (axisName) {
      case "x":
        return this.graphDiv._fullLayout.xaxis;
      case "y":
        return this.graphDiv._fullLayout.yaxis;
    }
  }

  onPointClick(...event) {
    console.log(event);
    this.getValuesFromCursor(event);
  }

  drawLabelForCursor(event) {
    this.startTime = new Date().getTime();
    const xy = this.getValuesFromCursor(event);
    if (!isNaN(xy.x)) {
      this.annotation.visible = true;
      this.annotation.x = xy.x;
      this.annotation.y = xy.y;
      this.annotation.text = `(${xy.x.toFixed(2)}, ${xy.y.toFixed(2)})`;
      Plotly.relayout(this.graphDiv, { annotations: [this.annotation] });
    }
  }

  drawThreshold(event) {
    this.startTime = new Date().getTime();
    console.log("drawThreshold");
    const xy = this.getValuesFromCursor(event);
    const xaxis = this.getAxis("x");
    const yaxis = this.getAxis("y");
    if (!isNaN(xy.x)) {
      this.layout.shapes = [];
      if (this.showThresholdForX) {
        this.thresholdX.x0 = xy.x;
        this.thresholdX.x1 = xy.x;
        this.thresholdX.y0 = yaxis.range[0];
        this.thresholdX.y1 = yaxis.range[1];
        this.layout.shapes.push(this.thresholdX);
      }
      if (this.showThresholdForY) {
        this.thresholdY.x0 = xaxis.range[0];
        this.thresholdY.x1 = xaxis.range[1];
        this.thresholdY.y0 = xy.y;
        this.thresholdY.y1 = xy.y;
        this.layout.shapes.push(this.thresholdY);
      }
      const pointsForGroups = this.splitPointsToGroups(xy);
      const newTraces = [];
      let groupInd = 0;
      for (const groupName in pointsForGroups) {
        if (pointsForGroups.hasOwnProperty(groupName)) {
          newTraces.push(
            this.createTrace(pointsForGroups[groupName], groupInd)
          );
        }
        groupInd++;
      }
      this.traces = newTraces;
      const cuurentTracesLen = this.graphDiv.data.length;
      const tracesToRemove = [];
      for (let i = 0; i < cuurentTracesLen; i++) {
        tracesToRemove.push(i);
      }

      console.log(
        "Assigning points to traces took:",
        new Date().getTime() - this.startTime,
        "ms"
      );

      // deleteTraces calls onUpdate method
      Plotly.deleteTraces(this.graphDiv, tracesToRemove);

      // addTraces calls onUpdate method
      this.startTime = new Date().getTime();
      Plotly.addTraces(this.graphDiv, this.traces);

      //this.graphDiv.data = this.traces;
      //Plotly.redraw(this.graphDiv);
    }
  }

  splitPointsToGroups(xy) {
    const groups = {
      "11": { x: [], y: [] },
      "-11": { x: [], y: [] },
      "-1-1": { x: [], y: [] },
      "1-1": { x: [], y: [] }
    };
    this.dataFromDB.forEach(data => {
      if (data.x >= xy.x && data.y >= xy.y) {
        groups["11"].x.push(data.x);
        groups["11"].y.push(data.y);
      } else if (data.x < xy.x && data.y >= xy.y) {
        groups["-11"].x.push(data.x);
        groups["-11"].y.push(data.y);
      } else if (data.x < xy.x && data.y < xy.y) {
        groups["-1-1"].x.push(data.x);
        groups["-1-1"].y.push(data.y);
      } else if (data.x >= xy.x && data.y < xy.y) {
        groups["1-1"].x.push(data.x);
        groups["1-1"].y.push(data.y);
      }
    });
    return groups;
  }

  createTrace(points, index) {
    const traceMarker = Object.assign({}, this.defaultMarker);
    traceMarker.color = this.colors[index];
    const trace = Object.assign({}, this.traceConfiguration, {
      x: points.x,
      y: points.y,
      marker: traceMarker
    });
    return trace;
  }

  onHover(event) {
    console.log("plotly_hover");
    console.log(event.points);
    const pointIndex = event.points[0].pointIndex;
    const traceIndex = event.points[0].curveNumber;
    this.traces[traceIndex].text[
      pointIndex
    ] = `Line 1<br>Line 2<br>random text ${parseInt(Math.random() * 100)}`;
    this.annotation.opacity = 0;
    //this.rerenderChart();
  }

  onUnHover(event) {
    console.log("plotly_unhover");
    this.annotation.opacity = 0.8;
    //this.rerenderChart();
  }

  onInitialized(figure, graphDiv) {
    console.log("onInitialized");
    this.graphDiv = graphDiv;

    graphDiv.addEventListener("mousemove", event => {
      console.log("TCL: Scatter -> onInitialized -> event", event);

      this.drawLabelForCursor(event);
    });
    graphDiv.addEventListener("click", event => {
      this.drawThreshold(event);
    });
    graphDiv.on("plotly_relayout", plotData => {
      console.log("plotly_relayout");
      //this.onZoom(plotData);
    });

    graphDiv.on("plotly_click", (...args) => {
      this.onPointClick(args);
    });
    graphDiv.on("plotly_hover", event => {
      this.onHover(event);
    });
    graphDiv.on("plotly_unhover", event => {
      this.onUnHover(event);
    });
    graphDiv.on("plotly_doubleclick", plotData => {
      console.log("plotly_doubleclick");
    });
  }

  onUpdate(figure, graphDiv) {
    //this.layoutRangeX = figure.layout.xaxis.range;
    //this.layoutRangeY = figure.layout.yaxis.range;
    console.log(
      "onUpdate",
      "RENDERING DATA TOOK:",
      new Date().getTime() - this.startTime,
      "ms"
    );
  }

  render() {
    const plot1 = React.createElement(this.Plot, {
      data: this.traces,
      config: { editable: true },
      layout: this.layout,
      onUpdate: (figure, graphDiv) => {
        this.onUpdate(figure, graphDiv);
      },
      onInitialized: (figure, graphDiv) => {
        this.onInitialized(figure, graphDiv);
      },
      key: "plot1"
      //revision: this.state.revision,
    });
    const container = React.createElement("div", {}, [plot1]);
    return container;
    /*return (
            <Plot
                graphDiv="graph"
                data={this.data1}
                revision={this.state.revision}
                layout={{width: 800, height: 600, title: 'A Fancy Plot'}}
                onUpdate= {(figure) => {this.onUpdate(figure)}}
            />
        );*/
  }
}
